# Laravel Vue Pagination

A simple numbered pagination for laravel and vue.
## Installation

- `Copy Laravel Vue Pagination to your project`
- `Import AnaVuePaginate component`
- `Pass the necessary props`

## Example
import AnaVuePaginate from "./AnaVuePaginate";

export default{

default(){
return{

publicationsObject:null

}

},
components:{
AnaVuePaginate
},

methods:{

getPublications(){

this.publicationsObject = paginatedResponseObject
}

}

template:`
<ana-vue-paginate v-if="publicationsObject" :pagination-object="publicationsObject" @update="getPublications"></ana-vue-paginate>

`

}